<h2>Dados do pedido</h2>

Código do pedido: {{ $pedido->codigo_pedido }} <br>
Data do pedido: {{ date('d/m/Y H:i:s', strtotime($pedido->data_pedido)) }} <br>
Forma pagamento: {{ $pedido->forma_pagamento }} <br>
Observações: {{ $pedido->observacao }}<br>

<br>
<br>

<h2>Dados do cliente</h2>
Nome: {{ $pedido->cliente->nome }}<br>
CPF: {{ $pedido->cliente->cpf }}<br>
Email: {{ $pedido->cliente->email }}<br>
Sexo: {{ $pedido->cliente->getSexoString() }}<br>

<br>
<br>

<h2>Produtos</h2>

@php
    $valorTotalPedido = 0
@endphp

@foreach ($pedido->produtos as $produto)

    @php $valorTotalProduto = $produto->pivot->quantidade * $produto->valor @endphp

    <hr>

    Nome do produto: {{ $produto->nome }} <br>
    Cor: {{ $produto->cor }} <br>
    Tamanho: {{ $produto->tamanho }} <br>
    Valor unitário: R$ {{ number_format($produto->valor, '2', ',', '.') }} <br>
    Quantidade: {{ $produto->pivot->quantidade }} <br>
    Valor: R$ {{ number_format($valorTotalProduto, '2', ',', '.') }} <br>

    <br>

    @php
        $valorTotalPedido += $valorTotalProduto 
    @endphp
@endforeach

<hr>

<h5></h5><strong>Valor total do pedido: R$ {{ number_format($valorTotalPedido, '2', ',', '.') }} </strong></h5>
<br>
<br>
<br>
Esperamos que você tenha gostado de comprar com a gente! <br>
Estaremos sempre à sua disposição! <br>
Agradecemos a preferência! <br>
<br>

Atenciosamente, <br>
Equipe <strong>VipCommerce!</strong> =)
