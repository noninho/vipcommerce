<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Clientes
$router->get('/clientes', 'ClienteController@getAll');
$router->get('/clientes/{id}', 'ClienteController@getById');
$router->post('/clientes', 'ClienteController@store');
$router->put('/clientes/{id}', 'ClienteController@update');
$router->delete('/clientes/{id}', 'ClienteController@destroy');

// Produtos
$router->get('/produtos', 'ProdutoController@getAll');
$router->get('/produtos/{id}', 'ProdutoController@getById');
$router->post('/produtos', 'ProdutoController@store');
$router->put('/produtos/{id}', 'ProdutoController@update');
$router->delete('/produtos/{id}', 'ProdutoController@destroy');

// Pedidos
$router->get('/pedidos', 'PedidoController@getAll');
$router->get('/pedidos/{id}', 'PedidoController@getById');
$router->post('/pedidos', 'PedidoController@storeUpdate');
$router->put('/pedidos/{id}', 'PedidoController@storeUpdate');
$router->delete('/pedidos/{id}', 'PedidoController@destroy');
$router->post('/pedidos/{id}/sendmail', 'PedidoController@sendmail');
$router->get('/pedidos/{id}/report', 'PedidoController@report');
$router->post('/pedidos/{id}/report', 'PedidoController@report');
