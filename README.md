# API de teste VipCommerce

Api de teste para a VipCommerce construída com framework Laravel/Lumen.

## Requisitos
1. PHP >= 7.2
2. DOM extension
3. MBString extension
4. MySQL

## Instruções de uso

1. Clonar projeto:
> git clone https://github.com/ronierisonsena/vipcommerce

2. Navegar ate a pasta do projeto e instalar as dependências:
> composer install

3. Dentro da pasta do projeto, copiar o arquivo .env.example para .env
> cp .env.example .env

4. No MySQL, Criar database de nome: "vipcommerce", com user: "root" e senha "toor". Ou definir o usuário e senha do seu banco de dados no arquivo ".env" localizado na raíz do projeto, através das variáveis DB_USERNAME e DB_PASSWORD respectivamente.

5. Dentro da pasta do projeto, rodar a migration para criar as tabelas:
> php artisan migrate

6. Dentro da pasta do projeto, rodar a seed para criar alguns registro nas tabelas users e produtos:
> php artisan db:seed

7. Servir a aplicação na porta desejada. Assumindo que o projeto esta sendo rodando na porta 80, navegue ate a pasta do projeto e rode o comando (usar com 'sudo' se necessário):
> php -S localhost:80 -t public

8. Para testes importar e rodar as collections do postman que estão inclusas na raíz deste projeto (rodar na ordem abaixo):
9. Clientes.postman_collection.json
10. Produtos.postman_collection.json
11. Pedidos.postman_collection.json

12. Para os testes através do PHPUnit, rodar o comando abaixo de dentro da pasta do projeto
> ./vendor/bin/phpunit

12. Para acessar o relatório do pedido após cria-lo pelo postman, acessar a URL abaixo (assumindo que o projeto está rodando na porta 80):
> http://localhost/pedidos/1/report
