<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ClienteTest extends TestCase
{
    // use DataBaseTransactions;

    /** 
     * Cria um novo cliente
     * 
     * @test */
    public function create_cliente()
    {
        $user = factory('App\Models\Cliente')->make();

        $response = $this->call('POST', '/clientes', $user->toArray());

        $this->assertEquals(201, $response->status());
    }

    /** 
     * Atualiza um cliente
     * 
     * @test */
    public function update_cliente()
    {
        $user = factory('App\Models\Cliente')->make();

        $response = $this->call('PUT', '/clientes/1', $user->toArray());

        $this->assertEquals(200, $response->status());
    }

    /** 
     * Busca todos os clientes
     * 
     * @test */
    public function get_cliente()
    {
        $response = $this->call('GET', '/clientes/1');

        $response->assertStatus(200);        
    }

    /** 
     * Busca todos os clientes
     * 
     * @test */
    public function get_all_clientes()
    {
        $response = $this->call('GET', '/clientes');

        $response->assertStatus(200);
    }

    /**
     * Delete um cliente
     * 
     * @test
     */
    // public function delete_cliente()
    // {
    //     $response = $this->call('delete', '/clientes/2');

    //     $response->assertStatus(200);
    // }
    
}
