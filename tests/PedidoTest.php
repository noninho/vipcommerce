<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class PedidoTest extends TestCase {

    // use DatabaseTransactions;

    /**
     * Cria um Pedido
     * 
     * @test
     */
    public function create_pedido()
    {
        $pedido = factory('App\Models\Pedido')->make();

        $response = $this->call('POST', '/pedidos', $pedido->toArray());

        $response->assertStatus(201);
    }


    /**
     * Atualizar Pedido
     * 
     * @test
     */
    public function update_pedido()
    {
        $pedido = factory('App\Models\Pedido')->make(["forma_pagamento" => 'cheque']);

        $response = $this->call('PUT', '/pedidos/1', $pedido->toArray());

        $response->assertStatus(200);
    }


    /**
     * Detalhes do pedido
     * 
     * @test
     */
    public function details_pedido()
    {
        $response = $this->call('GET', '/pedidos/1');
        
        $response->assertStatus(200);
    }

    /**
     * lista de pedidos
     * 
     * @test
     */
    public function get_all_pedidos()
    {
        $response = $this->call('GET', '/pedidos');

        $response->assertStatus(200);
    }

    /**
     * Deleta Pedido
     * 
     * @test
     */
    // public function delete_pedido()
    // {
    //     $response = $this->call('delete', '/pedidos/1');

    //     $response->assertStatus(200);
    // }


    /**
     * Testa o relatorio de pedido
     * 
     * @test
     */
    public function post_pedido_report()
    {
        $response = $this->call('POST', '/pedidos/1/report');

        $response->assertSee('cliente');
    }
}