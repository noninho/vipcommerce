<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ProdutoTest extends TestCase
{
    // use DataBaseTransactions;

    /**
     * Cria um produto
     * 
     * @test
     */
    public function create_produto()
    {
        $produto = factory('App\Models\Produto')->make();

        $response = $this->call('POST', '/produtos', $produto->toArray());

        $response->assertStatus(201);
    }

    /**
     * Atualiza um produto
     * 
     * @test
     */
    public function update_produto()
    {
        $produto = factory('App\Models\Produto')->make();

        $response = $this->call('PUT', '/produtos/1', $produto->toArray());

        $response->assertStatus(200);
    }

    /**
     * Exibe detalhes de um produto
     * 
     * @test
     */
    public function details_produto()
    {
        $response = $this->call('GET', '/produtos/1');

        $response->assertStatus(200);
    }

    /**
     * Exibe lista de produtos
     * 
     * @test
     */
    public function get_all_productos()
    {
        $response = $this->call('GET', '/produtos');

        $response->assertStatus(200);
    }


    /**
     * Deleta um produto
     * 
     * @test
     */
    // public function delete_produto()
    // {
    //     $response = $this->call('delete', '/produtos/2');

    //     $response->assertStatus(200);
    // }
}
