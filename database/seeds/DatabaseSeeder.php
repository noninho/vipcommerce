<?php

use Illuminate\Database\Seeder;
// use Illuminate\Database\Eloquent\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call('UsersTableSeeder');
        DB::insert('insert into clientes (codigo_cliente, nome, cpf, email, sexo) values (?, ?, ?, ?, ?)', [1, 'Ronierison', '10803182678', 'ronierisonsena@gmail.com', 'm']);
        DB::insert('insert into clientes (codigo_cliente, nome, cpf, email, sexo) values (?, ?, ?, ?, ?)', [2, 'User teste', '10803182378', 'teste@teste.com', 'm']);
        DB::insert('insert into produtos (codigo_produto, nome, cor, tamanho, valor) values (?, ?, ?, ?, ?)', [1, 'Cadeira gamer', 'Azul', 'Único', 189.90]);
        DB::insert('insert into produtos (codigo_produto, nome, cor, tamanho, valor) values (?, ?, ?, ?, ?)', [2, 'Mouse gamer', 'branco', 'Único', 39.90]);
    }
}
