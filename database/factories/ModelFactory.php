<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cliente;
use App\Models\Produto;
use App\Models\Pedido;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

// Cliente
$factory->define(Cliente::class, function (Faker $faker) {
    return [
        'nome' => $faker->name,
        'email' => $faker->email,
        'sexo' => 'm',
        'cpf' => 12 . $faker->randomNumber(9),
    ];
});

// Faker Produto
$factory->define(Produto::class, function ($faker) {
    return [
        'nome' => $faker->name,
        'cor' => $faker->colorName,
        'tamanho' => $faker->text(8),
        'valor' => $faker->randomFloat(2, 60.00, 999.99),
    ];
});

// Pedido
$factory->define(Pedido::class, function ($faker) {
    return [
        "data_pedido" => $faker->date('Y-m-d H:i:s'),
        "observacao" => $faker->text(100),
        "forma_pagamento" => "dinheiro",
        "codigo_cliente" => 1,
        "produtos" => [
            [
                "codigo_produto" => 1,
                "quantidade" => 2
            ]
        ]
    ];
});