<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->id('codigo_pedido');
            $table->dateTime('data_pedido');
            $table->string('observacao', 255)->nullable();
            $table->string('forma_pagamento', 10)->comment('Dinheiro|Cartão|Cheque');

            // cliente
            $table->bigInteger('codigo_cliente')->unsigned();
            $table->foreign('codigo_cliente')
                ->references('codigo_cliente')
                ->on('clientes')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->softDeletes();
            $table->timestamps();
        });

        
        Schema::create('pedido_produto', function (Blueprint $table) {
            $table->bigInteger('codigo_pedido')->unsigned();
            $table->foreign('codigo_pedido')
                ->references('codigo_pedido')
                ->on('pedidos')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            
            $table->bigInteger('codigo_produto')->unsigned();
            $table->foreign('codigo_produto')
                ->references('codigo_produto')
                ->on('produtos')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('quantidade');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pedido_produto');
        Schema::dropIfExists('pedidos');
    }
}
