<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pedido;
use App\Models\Produto;
use DB;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;

/**
 * Classe responsável pela regra de negócio dos pedidos
 */
class PedidoController extends Controller
{
    /**
     * Exibe a lista de todos os pedidos cadastrados.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        try {
            return response()->json(Pedido::orderBy('created_at')->get(), 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }


    /**
     * Exibe detalhes de um pedido.
     *
     * @param Integer $id
     * @return \Illuminate\Http\Response
     */
    public function getById($id)
    {
        try {
            $pedido = Pedido::where('codigo_pedido', $id)->with('produtos')->first();

            if (!$pedido) {
                return response()->json(["erro" => "Pedido não encontrado!"], 404);
            }

            return response()->json($pedido, 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Salva ou atualiza um novo pedido.
     *
     * @param  Integer $id - ID do pedido, caso for atualização
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeUpdate($id = null, Request $request)
    {
        // Aplica validações iniciais
        $this->validate(
            $request,
            Pedido::getValidationRules(),
            Pedido::getValidationMessages()
        );

        try {
            // Valida se todos os produtos existem no banco
            $produtosValidados = $this->validarProdutos($request);

            if (!$produtosValidados['success']) {
                return response()->json(
                    [
                        "produtos" => "Todos os produtos devem estar cadastrados no banco de dados!",
                        "codigos" => $produtosValidados['idsNaoEncontrados']
                    ], 422);
            }

            // Valida quantidades = 0
            $quantidadesValidadas = $this->validarQuantidade($request);

            if (!$quantidadesValidadas['success']) {
                return response()->json(
                    [
                        "produtos" => "Não pode haver produtos com quantidade = 0!",
                        "codigos" => $quantidadesValidadas['idsProdutosZerados']
                    ]
                    , 422);
            }

            // Cria array de produtos para associar ao pedido
            $produtos = $request->only('produtos')['produtos'];

            foreach ($produtos as $produto) {
                $arrayProdutoQuantidade[$produto['codigo_produto']] = ['quantidade' => $produto['quantidade']];
            }

            // inicia a transação
            $pedido = DB::transaction(function () use ($request, $arrayProdutoQuantidade, $id) {
                // Salva dados do pedido
                if ($id) {
                    $pedido = Pedido::find($id);
                    $pedido->update($request->except('produtos'));
                } else {
                    $pedido = Pedido::create($request->except('produtos'));
                }

                // Associa produtos e quantidades ao pedido
                $pedido->produtos()->sync($arrayProdutoQuantidade);

                return $pedido;
            });

            return response()->json($pedido, $id ? 200 : 201);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }


    /**
     * Deleta um pedido.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $pedido = Pedido::find($id);

            if (!$pedido) {
                return response()->json(["erro" => "Pedido não encontrado!"], 404);
            }

            // Deleta o pedido
            $pedido->delete();

            return response()->json("Pedido removido!", 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Envia email com os detalhes do pedido
     * 
     * @param Integer $id
     */
    public function sendmail($id, Request $request)
    {
        try {
            $pedido = Pedido::where('codigo_pedido', $id)->with('produtos')->first();

            if (!$pedido) {
                return response()->json(["erro" => "Pedido não encontrado!"], 404);
            }

            Mail::send('mail.emailPedido', ['pedido' => $pedido], function ($m) use ($pedido) {
                $m->from('noreply.vipcommerce@gmail.com', 'VipCommerce');
                $m->to($pedido->cliente->email);
                $m->subject('Detalhes pedido');
            });

            return response()->json("Email enviado!", 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Cria o relatório do pedido
     * 
     * @param Integer $id
     * @return JSON
     */
    public function report($id, Request $request)
    {
        try {
            $pedido = Pedido::where('codigo_pedido', $id)->with('produtos')->first();

            if (!$pedido) {
                return response()->json(["erro" => "Pedido não encontrado!"], 404);
            }

            // Verifica se é um POST, caso for, retorna os dados do pedido
            if ($request->isMethod('post')) {
                // Dados cliente
                $dadosPedido['cliente']['nome'] = $pedido->cliente->nome;
                $dadosPedido['cliente']['email'] = $pedido->cliente->email;
                $dadosPedido['cliente']['cpf'] = $pedido->cliente->cpf;
                $dadosPedido['cliente']['sexo'] = $pedido->cliente->getSexoString();
                
                // Dados pedido
                $dadosPedido['pedido']['codigo_pedido'] = $pedido->codigo_pedido;
                $dadosPedido['pedido']['data_pedido'] = $pedido->data_pedido;
                $dadosPedido['pedido']['forma_pagamento'] = $pedido->forma_pagamento;
                $dadosPedido['pedido']['observacao'] = $pedido->observacao;
                
                $valorTotalPedido = 0;

                // Produtos
                $i = 0;
                foreach ($pedido->produtos as $produto) {
                    $dadosPedido['pedido']['produtos'][$i]['codigo_produto'] = $produto->codigo_produto;
                    $dadosPedido['pedido']['produtos'][$i]['nome'] = $produto->nome;
                    $dadosPedido['pedido']['produtos'][$i]['cor'] = $produto->cor;
                    $dadosPedido['pedido']['produtos'][$i]['tamanho'] = $produto->tamanho;
                    $dadosPedido['pedido']['produtos'][$i]['valor'] = $produto->valor;
                    $dadosPedido['pedido']['produtos'][$i]['quantidade'] = $produto->pivot->quantidade;
                    $dadosPedido['pedido']['produtos'][$i]['valorTotal'] = $produto->pivot->quantidade * $produto->valor;

                    $valorTotalPedido += $produto->pivot->quantidade * $produto->valor;

                    $i++;
                }

                $dadosPedido['pedido']['valorTotal'] = $valorTotalPedido;

                return response()->json($dadosPedido, 200);
            }

            // Se request for GET, exibe o PDF no browser com opção de download
            $pdf = PDF::loadView('pdf.pdfPedido', compact('pedido'));

            return $pdf->stream('detalhes_pedido.pdf');
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }


    /**
     * Valida se todos os produtos do pedido estão cadastrados no banco de dados
     * 
     * @param \Illuminate\Http\Request  $request
     * @return Array
     */
    public function validarProdutos($request)
    {
        $retorno['success'] = true;
        $retorno['idsNaoEncontrados'] = [];

        $produtos = $request->only('produtos')['produtos'];
        
        // cria array de IDs de todos os produtos do pedido
        $idsProdutosPedido = array_map(function($produto) {
            return $produto['codigo_produto'];
        }, $produtos);

        // Busca no banco IDS de todos os produtos do pedido
        $idsProdutosValidos = Produto::whereIn('codigo_produto', $idsProdutosPedido)->pluck('codigo_produto')->toArray();

        // Cria array com IDS inexistentes no banco
        $idsNaoEncontrados = array_diff($idsProdutosPedido, $idsProdutosValidos);

        // Valida se todos os produtos existem no banco
        if (!empty($idsNaoEncontrados)) {
            $retorno['success'] = false;
            $retorno['idsNaoEncontrados'] = $idsNaoEncontrados;
        }

        return $retorno;
    }

    /**
     * Valida se existem produtos com quantidades = 0 no pedido
     * 
     * @param \Illuminate\Http\Request  $request
     * @return Array
     */
    public function validarQuantidade($request)
    {
        $retorno['success'] = true;
        $retorno['idsProdutosZerados'] = [];

        $produtos = $request->only('produtos')['produtos'];

        // Verifica se existe algum produto com quantidade=0
        $existeQuantidadeZerada = array_filter($produtos, function($produto) {
            return $produto['quantidade'] == 0;
        });

        if (!empty($existeQuantidadeZerada)) {
            $retorno['success'] = false;
            $retorno['idsProdutosZerados'] = $existeQuantidadeZerada;
        }

        return $retorno;
    }
}
