<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Produto;

/**
 * Classe responsável pela regra de negócio dos produtos
 */
class ProdutoController extends Controller
{
    /**
     * Retorna todos os produtos cadastrados.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {
        try{
            return response()->json(Produto::orderBy('created_at')->get(), 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Retorna detalhes de um produto.
     *
     * @param Integer $id
     * @return \Illuminate\Http\Response
     */
    public function getById($id, Request $request)
    {
        try{
            $produto = Produto::find($id);

            if (!$produto) {
                return response()->json(["erro" => "Produto não encontrado!"], 404);
            }

            return response()->json($produto, 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Salva um novo produto.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validações
        $this->validate(
            $request,
            Produto::getRulesValidation(),
            Produto::getValidationMessages()
        );

        try {
            // Persiste os dados
            $produto = Produto::create($request->all());

            return response()->json($produto, 201);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }


    /**
     * Atualiza um produto.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $produto = Produto::find($id);

        if (!$produto) {
            return response()->json(["erro" => "Produto não encontrado!"], 404);
        }

        // validações
        $this->validate(
            $request,
            Produto::getRulesValidation(),
            Produto::getValidationMessages()
        );

        try {
            $produto->update($request->all());

            return response()->json($produto, 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Deleta um produto.
     *
     * @param  Integer  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produto = Produto::find($id);

        if (!$produto) {
            return response()->json(["erro" => "Produto não encontrado!"], 404);
        }

        try {
            $produto->delete();

            return response()->json("Produto removido!", 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }
}
