<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Cliente;

use App\Http\Requests\ClienteRequest;

/**
 * Controller responsável pela regra de negócio dos clientes
 */
class ClienteController extends Controller
{

    /**
     * Exibe a lista de clientes cadastrados.
     *
     * @return JSON
     */
    public function getAll()
    {
        try {
            return response()->json(Cliente::orderBy('created_at')->get(), 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }

    /**
     * Exibe detalhes de um cliente.
     *
     * @param Integer $id
     * @return JSON
     */
    public function getById($id, Request $request)
    {
        try {
            $cliente = Cliente::find($id);

            if (!$cliente)
                return response()->json(["erro" => "Cliente não encontrado"], 404);

            return response()->json($cliente, 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }


    /**
     * Salva cliente.
     *
     * @return JSON
     */
    public function store(Request $request)
    {
        // Validações
        $this->validate(
            $request, 
            Cliente::getValidationRules(),
            Cliente::getValidationMessages()
        );

        try {
            // Persiste os dados
            $cliente = Cliente::create($request->all());

            return response()->json($cliente, 201);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
        
    }


    /**
     * Atualiza dados de um cliente.
     *
     * @param Integer $id
     * @return JSON
     */
    public function update($id, Request $request)
    {
        $cliente = Cliente::find($id);

        if (!$cliente)
            return response()->json(["erro" => "Cliente não encontrado"], 404);

        // Validações
        $this->validate(
            $request, 
            Cliente::getValidationRules($id),
            Cliente::getValidationMessages()
        );

        try {
            // Persiste os dados
            $cliente->update($request->all());

            return response()->json($cliente, 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }


    /**
     * Deleta um cliente.
     *
     * @param  Integer  $id
     * @return JSON
     */
    public function destroy($id)
    {
        $cliente = Cliente::find($id);

        if (!$cliente)
            return response()->json(["erro" => "Cliente não encontrado"], 404);

        try {
            // Remove
            $cliente->delete();

            return response()->json("Cliente removido!", 200);
        } catch (\Exception $e) {
            return response()->json([
                "erro" => $e->getMessage()
            ], 500);
        }
    }
}
