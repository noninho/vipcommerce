<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Classe responsável pelos Produtos
 */
class Produto extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'codigo_produto',
        'nome',
        'cor',
        'tamanho',
        'valor',
    ];

    protected $primaryKey = 'codigo_produto';


    public static function getRulesValidation()
    {
        return [
            'nome' => 'required|string|max:100',
            'cor' => 'required|string|max:30',
            'tamanho' => 'required|string|max:30',
            'valor' => 'required|numeric|regex:/^\d+(\.\d{1,2})?$/',
        ];
    }


    public static function getValidationMessages()
    {
        return [
            'required' => 'O campo :attribute é obrigatório!',
            'string' => 'O campo :attribute deve ser uma string!',
            'numeric' => 'O campo :attribute deve ser um número!',
            'regex' => 'O campo :attribute deve estar no formato: 1000.00',
        ];
    }

    /**
     * Relações
     */
    public function pedidos()
    {
        return $this->belongsToMany('App\Models\Pedido', 'pedido_produto', 'codigo_produto', 'codigo_pedido')->withPivot('quantidade');
    }
}
