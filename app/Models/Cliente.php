<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Classe responsável pelos clientes
 */
class Cliente extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'codigo_cliente',
        'nome',
        'cpf',
        'sexo',
        'email',
    ];

    protected $primaryKey = 'codigo_cliente';

    /**
     * Regras de validação do cliente
     * 
     * @return Array
     */
    public static function getValidationRules($id = null)
    {
        $rules = [
            'nome' => 'required|string|max:80',
            'email' => 'required|email|unique:clientes,email,NULL,codigo_cliente,deleted_at,NULL',
            'cpf' => 'required|numeric|digits:11|unique:clientes,cpf,NULL,codigo_cliente,deleted_at,NULL',
            'sexo' => array('required', 'string', 'size:1', 'regex:/(m|f)/')
        ];

        // Update, modifica regras de email e cpf únicos
        if ($id) { 
            $rules['email'] = "required|email|unique:clientes,email,{$id},codigo_cliente";
            $rules['cpf'] = "required|numeric|digits:11|unique:clientes,cpf,{$id},codigo_cliente,deleted_at,NULL";
        }

        return $rules;
    }

    /**
     * Mensagens de validação
     * 
     * @return Array
     */
    public static function getValidationMessages()
    {
        return [
            'required' => 'O campo :attribute é obrigatório!',
            'nome.max' => 'O campo :attribute deve ter no máximo 80 caracteres!',
            'string' => 'O campo :attribute deve ser string!',
            'unique' => 'Esse :attribute ja está cadastrado!',
            'numeric' => 'O campo :attribute deve conter somente numeros!',
            'email' => 'O campo email deve ser um email válido!',
            'digits' => 'O campo :attribute deve conter 11 caracteres!',
            'sexo.regex' => "O campo :attribute deve conter somente 1 caractere! ('m' para masculino ou 'f' para feminino)",
            'sexo.size' => "O campo :attribute deve conter somente 1 caractere! ('m' para masculino ou 'f' para feminino)",
        ];
    }


    public function getSexoString()
    {
        return strtolower($this->sexo) == 'm' ? 'Masculino' : 'Feminino';
    }

    /**
     * Relações
     */
    public function pedidos()
    {
        return $this->hasMany('App\Models\Pedido', 'codigo_cliente');
    }
}
