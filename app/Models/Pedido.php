<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pedido extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'data_pedido',
        'observacao',
        'forma_pagamento', # Dinheiro|Cartão|Cheque
        'codigo_cliente'
    ];

    protected $primaryKey = 'codigo_pedido';

    // Regras de validação
    public static function getValidationRules()
    {
        return [
            'data_pedido' => 'required|date_format:Y-m-d H:i:s',
            'forma_pagamento' => array('required', 'string', 'regex:/^(dinheiro|cart(a|ã)o|cheque)$/'),
            'codigo_cliente' => 'required|numeric|min:1|exists:clientes,codigo_cliente',
            'produtos' => 'required' //|array|exists:produtos,codigo_produto'
        ];
    }

    // Mensagens de validação
    public static function getValidationMessages()
    {
        return [
            'required' => 'O campo :attribute é obrigatório!',
            'date_format' => 'O campo :attribute deve ser uma data válida no formato: YYYY-MM-DD H:i:s',
            'forma_pagamento.regex' => 'As formas de pagamento aceitas são: dinheiro, cartão ou cheque',
            'codigo_cliente.required' => 'O campo :attribute deve ser um cliente cadastrado!',
            'codigo_cliente.exists' => 'O campo :attribute deve ser um cliente cadastrado!',
            'codigo_cliente.min' => 'O campo :attribute deve ser um cliente cadastrado!',
            'numeric' => 'O campo :attribute deve ser um numero inteiro!',
            'produtos.required' => 'O campo :attribute deve ser um array de codigo_produto => quantidade! Ex: produtos: [{codigo_produto:PRODUTO_ID, quantidade:X }, {...}]'
            // 'produtos.array' => 'O campo :attribute deve ser um array de codigo_produto!',
            // 'produtos.exists' => 'O campo :attribute deve ser um array de produtos cadastrados!',
        ];
    }


    /**
     * Relações
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente', 'codigo_cliente');
    }

    public function produtos()
    {
        return $this->belongsToMany('App\Models\Produto', 'pedido_produto', 'codigo_pedido', 'codigo_produto')->withPivot('quantidade');
    }
}
